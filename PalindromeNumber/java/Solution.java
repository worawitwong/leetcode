class Solution {
    public boolean isPalindrome(int x) {
        if(x<10 && x>0) return true;
        if(x==0) return true;
        int input=x;
        int test = 0;
        if(x<0 || input==10) return false;
        while(x>0){
         test= (test*10) + (x%10);
         x=x/10;
        }
     if (test==input) return true;
     else return false;
    }
}